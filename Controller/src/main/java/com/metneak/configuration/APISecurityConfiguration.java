package com.metneak.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class APISecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //IN MEMORY
        auth.inMemoryAuthentication()
                .withUser("Me?tn@k@piV3")
                .password("{noop}m&t#e@k@piV@3")
                .roles("API");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //to become a basic authentication
        http.httpBasic();
        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
        http.csrf().disable();
        //to had session stateless (use key to clear catch of session)
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //To protected endpoint
//        http.authorizeRequests().antMatchers("/api/**").hasRole("API");
    }

}
