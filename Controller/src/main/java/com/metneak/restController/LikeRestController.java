package com.metneak.restController;

import com.metneak.repository.model.Like;
import com.metneak.repository.model.Post;
import com.metneak.service.LikeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/")
@Api(value = "Like Rest",tags ={"v2/api/likes"},description = "like crud")
public class LikeRestController {

    @Autowired
    LikeService likeService;

    @ApiOperation("TO DO: TO INSERT LIKE")
    @GetMapping("/like/new")
    public ResponseEntity<Map<String, Object>> insertLike(@RequestParam("user_id") Integer user_id, @RequestParam("post_id") Integer post_id){

        Map<String, Object> response = new HashMap<>();
        if (likeService.insertLike(user_id, post_id)) {
            response.put("status", true);
            response.put("message", "Like is successfully added!");
            response.put("data", likeService.insertLike(user_id, post_id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            response.put("status", false);
            response.put("message", "Failed");
            return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
        }

    }



    @ApiOperation(("TO DO: To Unlike "))
    @DeleteMapping ("/like/remove")
    public  ResponseEntity<Map<String,Object>> unlike(@RequestParam("user_id") Integer user_id, @RequestParam("post_id") Integer post_id){

        Map<String,Object> response = new HashMap<>();
        if(likeService.unlike(user_id,post_id)){
            response.put("status", true);
            response.put("message", "Deleted Successfully!");
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else {
            response.put("status", false);
            response.put("message", "Deleted Fail.");
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
    }



    @ApiOperation(("TO DO : SHOW ALL LIKERS"))
    @GetMapping("/like/show")
    public  ResponseEntity<Map<String,Object>> show( @RequestParam("post_id") Integer post_id) {

        Map<String, Object> response = new HashMap<>();
        List<Like> likes = likeService.getAllLike(post_id);
        if (likes == null) {
            response.put("status", false);
            response.put("message", "Show liker fail!");
            return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            response.put("status", true);
            response.put("message", "Successfully!");
            response.put("Data", likes);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }


    }
    }



