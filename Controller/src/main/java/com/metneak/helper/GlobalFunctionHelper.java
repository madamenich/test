package com.metneak.helper;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

public class GlobalFunctionHelper {
//    private static final java.util.UUID UUID = ;

    public static String uploaded(MultipartFile files) throws Exception {

        String fileName = UUID.randomUUID() + "." + files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf(".") + 1);

            Files.copy(files.getInputStream(), Paths.get("Controller/src/main/resources/static/user-img/",fileName));


        return fileName;
    }
}
