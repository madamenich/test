package com.metneak.repository;

import com.metneak.repository.model.Like;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeRepository {

    @Insert(" INSERT INTO public.mn_likes(user_id, post_id) VALUES (#{user_id}, #{post_id)}")
    boolean insertLike(@Param("user_id") Integer user_id, @Param("post_id") Integer post_id);

    @Delete("DELETE FROM public.mn_likes WHERE user_id= #{user_id} AND post_id =#{post_id}")
    boolean unlike(@Param("user_id")Integer user_id, @Param("post_id") Integer post_id);


    @Select("SELECT mn_likes.post_id,mn_users.name, mn_users.pro_picture FROM mn_likes " +
            "INNER JOIN mn_users ON mn_likes.user_id = mn_users.id " +
            "WHERE mn_likes.post_id = #{post_id} ")
    @Results({
        @Result(property = "id",column = "post_id"),
            @Result(property = "user.name",column = "name"),
            @Result(property = "user.image",column = "pro_picture"),
    })

   List<Like>getAllLike(@Param("post_id") Integer post_id);



}
