package com.metneak.repository;

import com.metneak.repository.model.Group;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository {



    @Select("select * from mn_groups where school_id = #{school_id} AND year = #{year} ")
    List<Group> ifGroupIsExist(Integer year,Integer school_id);

    @Insert(" INSERT INTO public.mn_groups(year, school_id) VALUES (#{year}, #{school_id})")
    boolean insert(@Param("year") Integer year,@Param("school_id") Integer school_id);

    @Insert("INSERT INTO public.mn_user_groups( user_id, group_id)VALUES ( #{user_id}, #{group_id})")
    boolean insertMember(Integer user_id, Integer group_id);

    @Select("SELECT g.id as id ,g.year as year,s.school_name as name FROM mn_groups g join mn_schools s on g.school_id = s.id where g.id in (#{group0},#{group1},#{group2}) ")
    @Results(
        @Result(property = "school.name" ,column = "name")
            )
    List<Group> findGroups(@Param("group0")Integer group0, @Param("group1")Integer group1, @Param("group2")Integer group2);


}
