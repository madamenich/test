package com.metneak.repository.model;

import java.sql.Date;

/**
 * Like
 */
public class Like {

    Integer id;
    User user;
    Date time;

    public Like() {
    }

    public Like(User user, Date time) {
        this.user = user;
        this.time = time;
    }

    public Like(Integer id, User user, Date time) {
        this.id = id;
        this.user = user;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Like{" +
                "id=" + id +
                ", user=" + user +
                ", time=" + time +
                '}';
    }
}