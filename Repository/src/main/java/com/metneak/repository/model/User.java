package com.metneak.repository.model;

import org.apache.ibatis.annotations.Result;
import org.postgresql.jdbc.PgArray;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * User
 */
public class User {

    int id;
    String name;
    String image;
    String cover;
    String gender;
    Date birthdate;
    String phone;
    Integer group[];
    Integer location_id;

    public User() {
    }

    public User(int id, String name, String image, String cover, String gender, Date birthdate, String phone, Integer group[], Integer location_id) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.cover = cover;
        this.gender = gender;
        this.birthdate = birthdate;
        this.phone = phone;
        this.group = group;
        this.location_id = location_id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCover() {
        return this.cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthdate() {
        return this.birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer[] getGroup() {
        return this.group;
    }

    public Integer getGroup1(){
        return this.group[0];
    }
    public Integer getGroup2(){
        return this.group[1];
    }
    public Integer getGroup3(){
        return this.group[2];
    }
    public void setGroup(Object group) {
        try {
          PgArray pr=  (PgArray) group;

              this.group = (Integer[]) pr.getArray();
    //             this.group = (Integer[]) ((PgArray)group).getArray();

         } catch (SQLException e) {
             System.out.println("not work here");
            e.printStackTrace();
         }
    }
    public void setGroupBy( Integer g1,Integer g2,Integer g3) {
        this.group = new Integer[3];
//        this.group = {g1,g2,g3};
        this.group[0] = g1;
        this.group[1] = g2;
        this.group[2] = g3;
    }


    public Integer getLocation_id() {
        return this.location_id;
    }

    public void setLocation_id(Integer location_id) {
        this.location_id = location_id;
    }

    public User id(int id) {
        this.id = id;
        return this;
    }

    public User name(String name) {
        this.name = name;
        return this;
    }

    public User image(String image) {
        this.image = image;
        return this;
    }

    public User cover(String cover) {
        this.cover = cover;
        return this;
    }

    public User gender(String gender) {
        this.gender = gender;
        return this;
    }

    public User birthdate(Date birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public User phone(String phone) {
        this.phone = phone;
        return this;
    }

    public User group(Integer group[]) {
        this.group = group;
        return this;
    }

    public User location_id(Integer location_id) {
        this.location_id = location_id;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", image='" + getImage() + "'" +
            ", cover='" + getCover() + "'" +
            ", gender='" + getGender() + "'" +
            ", birthdate='" + getBirthdate() + "'" +
             ", group='[" + getGroup() + "]'" +
            ", phone='" + getPhone() + "'" +
            ", location_id='" + getLocation_id() + "'" +
            "}";
    }

    
}