package com.metneak.service;

import com.metneak.repository.PostRepository;
import com.metneak.repository.model.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
    @Autowired
     public PostRepository postRepository;

    public List<Post> showAllPostsByGroup(Integer group_id){
        List<Post> posts =  postRepository.showAllPostsByGroup(group_id);
        return posts;
    }
    public List<Post> showAllPosts() {
        return postRepository.showAllPosts();
    }

    public List<Comment> showCommentById(int id) {
        return postRepository.showCommentById(id);
    }

    public List<Like> showLikeById(int id) {
        return postRepository.showLikeById(id);
    }

    public List<Post> showPostByGroupId(int id) {
        return postRepository.showPostByGroupId(id);
    }

    public boolean editPostById(String detail, int id){
        return postRepository.editPostById(detail,id);
    }

    public boolean insert(Post post){
        return postRepository.insert(post);
    }

    //delete post
    public boolean deletePostById(int id){
        return postRepository.deletePostById(id);
    }

    //user(id)
    public List<User>showUserById(int id){return postRepository.showUserById(id);}


    //show post by user id
    public List<Post> showPostByUserId(int id){
        return postRepository.showPostByUserId(id);
    }
    //show post by user id and group id
    public List<Post>showPostByGroupIdUserId(Integer group_id,Integer user_id){
        return postRepository.showPostByGroupIdUserId(group_id,user_id);
    }

}
